import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Header from "./Components/Header/Header";
import CardList from "./Components/CardList/CardList";
import About from "./Components/About/About";
import Details from "./Components/Details/Details";
import NotFound from "./Components/NotFound/NotFound";

const App: React.FC = () => {
  return (
    <Router>
      <Header />
      <Switch>
        <Route path="/" exact component={CardList} />
        <Route path="/about" component={About} />
        <Route path="/details/:id" component={Details} />
        <Route component={NotFound} />
      </Switch>
    </Router>
  );
};

export default App;
