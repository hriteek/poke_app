import * as React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Search from "../Search/Search";
// interface WelcomeProps {
//   title?: string;
// }

// const Header: React.FC<WelcomeProps> = ({ title }) => {
const Header: React.FC<{ title?: string }> = ({ title }) => {
  return (
    <div className="bg-blue-700 flex justify-around items-center min-h-full min-height">
      <Search />
      <div>
        <Link to="/">
          <h1 className="text-2xl text-white font-bold cursor-pointer">
            {title}
          </h1>
        </Link>
      </div>
      <FontAwesomeIcon icon="bars" className="text-white cursor-pointer" />
    </div>
  );
};

Header.defaultProps = {
  title: "POKE APP"
};
export default Header;
