import * as React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Search: React.FC = () => {
  return (
    <div className="flex ">
      <div className="">
        <input
          type="text"
          name="search"
          id="search"
          className=" pl-2 search"
          placeholder="Search"
        />
        <button
          type="submit"
          className="border-solid bg-white pr-2 negative-search"
        >
          <FontAwesomeIcon icon="search" className="text-black" />
        </button>
      </div>
    </div>
  );
};
export default Search;
