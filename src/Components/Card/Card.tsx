import React from "react";
import { Link } from "react-router-dom";

const Card: React.FC<{ name?: string; image?: string; id: number }> = ({
  name,
  image,
  id
}) => {
  return (
    <div className="border-solid border-black border-2 rounded-lg flex w-1/4 m-5 flex-col text-center">
      <div className="self-center">
        <Link to={`/details/${id}`}>
          <img src={image} alt={name} className="cursor-pointer" />
        </Link>
      </div>
      <h1 className="text-bold">{name}</h1>
    </div>
  );
};

// Card.defaultProps = {
//   name: "",
//   image: ""
// };

export default Card;
