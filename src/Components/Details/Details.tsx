import React, { useState } from "react";
import { RouteComponentProps } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";

const getPokemon = (pokemonId: string): PokemonObject => {
  const pokemons = require("../../data.json");
  const pokemon = pokemons.filter(
    (e: { id: number }) => e.id.toString() === pokemonId
  );
  return pokemon[0];
};

// const isEmpty = (obj: PokemonObject): boolean => {
//   for (let prop in obj) {
//     return true;
//   }
//   return false;
// };

export interface PokemonObject {
  id: number;
  num: string;
  name: string;
  img: string;
  height: string;
  weight: string;
  candy: string;
  candy_count: number;
  egg: string;
  spawn_chance: number;
  avg_spawns?: number;
  spawn_time?: string;
  type?: Array<string>;
  multipliers?: Array<number>;
  weaknesses?: Array<string>;
  next_evolution?: Array<{ num: string; name: string }>;
}

interface IDetailsProps extends RouteComponentProps<{ id: string }> {}

const Details: React.FC<IDetailsProps> = props => {
  const { id } = props.match.params;
  const [pokemon, setPokemon] = useState<PokemonObject>(getPokemon(id));
  console.log("pokemon", pokemon);

  return (
    <div className="bg-blue-700 h-screen">
      <Link to="/">
        <FontAwesomeIcon
          icon="arrow-left"
          className=" ml-5 text-white text-3xl"
        />
      </Link>
      <div className="flex text-center flex-col bg-blue-200  w-1/3 p-5 m-auto  rounded-lg">
        <div className="self-center">
          <img src={pokemon.img} alt={pokemon.name} />
        </div>
        <h1 className="text-4xl">{pokemon.name}</h1>
        <p>Height: {pokemon.height}</p>
        <p>Weight: {pokemon.weight}</p>
        <p>Egg: {pokemon.egg}</p>
        <p>Spawn chance: {pokemon.spawn_chance}</p>
        <p>Average spawns: {pokemon.avg_spawns}</p>
        <p>Spawn time: {pokemon.spawn_time}</p>
      </div>
    </div>
  );
};
export default Details;
