import React, { useState, useEffect } from "react";
// import axios from "axios";

import Card from "../Card/Card";

// simple function that shuffles the given array
const shuffle = (array: any[]) => {
  let currentIndex = array.length,
    temporaryValue,
    randomIndex;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
};

const CardList: React.FC = () => {
  const [pokemons, setPokemons] = useState([]);

  useEffect(() => {
    setPokemons(require("../../data.json"));
  }, []);

  if (pokemons.length === 0) {
    return <h1>Loading...</h1>;
  } else {
    return (
      <div className="flex justify-center flex-wrap m-auto">
        {shuffle(pokemons).map(
          (pokemon: {
            name: string | undefined;
            img: string | undefined;
            id: number;
          }) => (
            <Card
              key={pokemon.name}
              name={pokemon.name}
              image={pokemon.img}
              id={pokemon.id}
            />
          )
        )}
      </div>
    );
  }
};
export default CardList;
